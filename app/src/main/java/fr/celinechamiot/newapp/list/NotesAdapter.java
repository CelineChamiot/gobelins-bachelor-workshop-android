package fr.celinechamiot.newapp.list;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Celine on 20/10/2016.
 */

public class NotesAdapter extends RecyclerView {
    public NotesAdapter(Context context) {
        super(context);
    }

    public NotesAdapter(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NotesAdapter(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
